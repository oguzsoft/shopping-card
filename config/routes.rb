Rails.application.routes.draw do
  devise_for :admins
  devise_for :users 
  resources :categories 

  # resources :users do
  #   resources :products do
  #     member do
  #       get :add_to_cart
  #     end
  #   end
  # end

  resources :users do
    resources :products do
     # member do
     #   get :add_to_cart
     # end
    end

    resources :suppliers 
    resources :carts do
      resources :cartships
      member do
        get :add_to_cartship
        post :pay
        post :remove_item
      end
    end
  end

# resources :users do
#   resources :suppliers 
# end


  

  get 'sales/index'

  #root to: redirect('/users/sign_in')

  root 'sales#index'
end
