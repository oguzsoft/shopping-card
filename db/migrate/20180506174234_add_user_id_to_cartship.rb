class AddUserIdToCartship < ActiveRecord::Migration[5.2]
  def change
    add_column :cartships, :user_id, :integer
    add_index :cartships, :user_id
  end
end
