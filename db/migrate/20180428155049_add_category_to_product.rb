class AddCategoryToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :category, :integer
    add_index :products, :category
  end
end
