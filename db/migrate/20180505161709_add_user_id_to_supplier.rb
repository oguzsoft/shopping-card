class AddUserIdToSupplier < ActiveRecord::Migration[5.2]
  def change
    add_column :suppliers, :user_id, :integer
    add_index :suppliers, :user_id
  end
end
