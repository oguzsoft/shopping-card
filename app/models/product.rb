class Product < ApplicationRecord
  belongs_to :category
  belongs_to :supplier
  belongs_to :user
  validates :title, presence: true
  validates :price, presence: true
end
