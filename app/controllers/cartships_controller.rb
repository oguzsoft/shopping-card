class CartshipsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cart
  before_action :set_cartship, only: [:show, :edit, :update, :destroy]

  # GET /cartships
  # GET /cartships.json
  def index
    @cartships = @cart.cartships.all
  end

  # GET /cartships/1
  # GET /cartships/1.json
  def show
  end

  # GET /cartships/new
  def new
    #@quantity = 1
    #@cartship = @cart.cartships.create
    @cart = Cart.first
    #raise "#{current_user.products.find(params[:product_id])}"
    @product = current_user.products.find(params[:product_id])
   
    @cartship = @cart.cartships.create!(cart_id: @cart, product_id: @product.id, quantity: 1, user_id: current_user.id)
  
    redirect_to root_path

  end

  # GET /cartships/1/edit
  def edit
    @quantity = @cartship.quantity
  end

  # POST /cartships
  # POST /cartships.json
  def create
    # raise "#{cartship_params}"
    @cartship = @cart.cartships.new(cartship_params)

    #raise "#{cartship_params}"
    respond_to do |format|
      if @cartship.save!
        
        format.html { redirect_to root_url, notice: 'Cartship was successfully created.' }
        format.json { render :show, status: :created, location: @cartship }
      else
        format.html { render :new }
        format.json { render json: @cartship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cartships/1
  # PATCH/PUT /cartships/1.json
  def update
    respond_to do |format|
      if @cartship.update(cartship_params)
        format.html { redirect_to root_path, notice: 'Cartship was successfully updated.' }
        format.json { render :show, status: :ok, location: @cartship }
      else
        format.html { render :edit }
        format.json { render json: @cartship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cartships/1
  # DELETE /cartships/1.json
  def destroy
    @cartship.destroy
    respond_to do |format|
      if @cart.cartships.any?
        format.html { redirect_to root_path, notice: 'Cartship was successfully destroyed.' }
      else
        format.html { redirect_to root_url, notice: 'Cartship was successfully destroyed.' }        
      end
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = current_user.carts.first
    end

    def set_cartship
      @cartship = current_user.carts.first.cartships.find(params[:id])
    end
    

    # Never trust parameters from the scary internet, only allow the white list through.
    def cartship_params
      params.require(:cartship).permit(:cart_id, :user_id, :product_id, :quantity)
    end
end