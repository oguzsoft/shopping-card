class SalesController < ApplicationController
  def index
    @products = Product.order(:title).to_a
    @cart = Cart.first if Cart.any?
    @cartships = @cart.cartships.all
    #@products = current_user.products.order(:title).to_a
   

  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_params
      params.require(:sales).permit(:cart_id)
    end  
end