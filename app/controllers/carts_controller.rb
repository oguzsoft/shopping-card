class CartsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cart, only: [:show, :edit, :update, :destroy, :pay]

  # GET /carts
  # GET /carts.json
  def index
    @carts = current_user.carts.all
  end

  # GET /carts/1
  # GET /carts/1.json
  def show
  end

  # GET /carts/new
  def new
    Cart.create! user_id: current_user.id, status: "pending"    
    redirect_to new_user_cart_cartship_path(user_id: current_user.id, cart_id: Cart.first.id, product_id: params[:product_id])
  end


  # GET /carts/1/edit
  def edit
  end

  # POST /carts
  # POST /carts.json
  def create
    @cart = current_user.carts.new(cart_params)

    respond_to do |format|
      if @cart.save
        
        format.html { redirect_to @cart, notice: 'Cart was successfully created.' }
        format.json { render :show, status: :created, location: @cart }
      else
        format.html { render :new }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carts/1
  # PATCH/PUT /carts/1.json
  def update
    respond_to do |format|
      if @cart.update(cart_params)
        format.html { redirect_to @cart, notice: 'Cart was successfully updated.' }
        format.json { render :show, status: :ok, location: @cart }
      else
        format.html { render :edit }
        format.json { render json: @cart.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carts/1
  # DELETE /carts/1.json
  def destroy
    @cart.destroy

    respond_to do |format|
      format.html { redirect_to sales_index_url, notice: 'Cart was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def pay

    @cart.cartships.each do |cartship| 
      new_total = cartship.product.quantity - cartship.quantity
      cartship.product.update! quantity: new_total
    end

    @cart.destroy
    redirect_to root_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cart
      @cart = current_user.carts.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cart_params
      params.require(:cart).permit(:status, :user_id, :product_id)
    end

end
